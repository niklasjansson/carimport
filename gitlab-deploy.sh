
#Get servers list
set -f
string=$DEPLOY_SERVER
array=(${string//,/ })
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"do    
      echo "Deploy project on server ${array[i]}"    
      ssh ec2-user@${array[i]} "cd /home/application/carimport && git clone https://gitlab.com/niklasjansson/carimport.git"
done