DROP TABLE IF EXISTS taxvalues;

create table taxvalues 
(    
    id INT NOT NULL AUTO_INCREMENT,
    manufacturer VARCHAR(20) NOT NULL,
    model VARCHAR(50) NOT NULL,
    engine_size DECIMAL(2,1) NOT NULL,
    fuel VARCHAR(10) NOT NULL,
    engine_power DECIMAL(4,0) NOT NULL,
    specification VARCHAR(300) NOT NULL,
    decision_date DATE NOT NULL,
    registration_date DATE NOT NULL,
    mileage INT NOT NULL,
    domestic_value DECIMAL(9,2) UNSIGNED NOT NULL,
    vat_amount DECIMAL (9,2) UNSIGNED NOT NULL,
    PRIMARY KEY (id)
);
