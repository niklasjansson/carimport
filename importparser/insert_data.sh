#!/bin/bash
DATABASE=$1
SQLFILE=$PWD/output/tax-values-2018-2019-all.sql

sudo mysql "${DATABASE}" < create_table.sql
sudo mysql "${DATABASE}" < "${SQLFILE}"

exit

