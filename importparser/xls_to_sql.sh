#!/bin/bash
OUTPUT=$PWD/output
INPUT=$PWD/input

DIESEL="[0-9]\{1\}\.[0-9]\{1\}.D"
HYBRID="[0-9]\{1\}\.[0-9]\{1\}.H"
ELECTRIC=";0\.0"
FILTER="[0-9]\{2\}KW"

[ -d "${INPUT}" ] || mkdir "${INPUT}"
[ -d "${OUTPUT}" ] || mkdir "${OUTPUT}"

rm "${INPUT}"/*.xls
rm "${INPUT}"/*.csv
rm "${OUTPUT}"/*.sql

cd "${INPUT}"

wget https://www.vero.fi/contentassets/f3326d05d3954bed9c1686a17917754d/vuosi-2018_autot.xls
wget https://www.vero.fi/contentassets/f3326d05d3954bed9c1686a17917754d/vuosi-2019-autot.xls

ssconvert -S -O 'separator=;' vuosi-2018_autot.xls tax-values-2018.txt
ssconvert -S -O 'separator=;' vuosi-2019-autot.xls tax-values-2019.txt

#COLNAMES=$(grep -E ';;;;;|Märke|Merkki' tax-values_201*.txt.* | sed 's/"//g' | sed 's/;;/;/g'| sort -k1 | egrep 'Märke' | sort -u)
grep -Ehv ';;;|Märke|Merkki' tax-values-201*.txt.* | sed 's/"//g' | sed 's/;;/;/g' | sort -k1 > tax-values-2018-2019-raw.csv

# Remove some unstructured
grep -h "${FILTER}" tax-values-2018-2019-raw.csv | sort -k1 > tax-values-2018-2019-filtered.csv

grep -hv "${DIESEL}" tax-values-2018-2019-filtered.csv | grep -v "${HYBRID}" | grep -v "${ELECTRIC}" | sort -k1 > tax-values-2018-2019-petrol.csv
awk -F';' '{print "insert into taxvalues values (default,\x27" $1 "\x27,\x27" $2 "\x27,\x27" substr($3,0,3) "\x27,\x27\Petrol\x27,\x27" substr($3,length($3)-4,3) "\x27,\x27" $3 "\x27,\x27" $4 "\x27,\x27" $5 "\x27,\x27" $6 "\x27,\x27" $7 "\x27,\x27" $8 "\x27);"}' tax-values-2018-2019-petrol.csv > "${OUTPUT}"/tax-values-2018-2019-petrol.sql

grep -h "${DIESEL}" tax-values-2018-2019-filtered.csv | sort -k1 > tax-values-2018-2019-diesel.csv
awk -F';' '{print "insert into taxvalues values (default,\x27" $1 "\x27,\x27" $2 "\x27,\x27" substr($3,0,3) "\x27,\x27\Diesel\x27,\x27" substr($3,length($3)-4,3) "\x27,\x27" $3 "\x27,\x27" $4 "\x27,\x27" $5 "\x27,\x27" $6 "\x27,\x27" $7 "\x27,\x27" $8 "\x27);"}' tax-values-2018-2019-diesel.csv > "${OUTPUT}"/tax-values-2018-2019-diesel.sql

grep -h "${HYBRID}" tax-values-2018-2019-filtered.csv | sort -k1 > tax-values-2018-2019-hybrid.csv
awk -F';' '{print "insert into taxvalues values (default,\x27" $1 "\x27,\x27" $2 "\x27,\x27" substr($3,0,3) "\x27,\x27\Hybrid\x27,\x27" substr($3,length($3)-4,3) "\x27,\x27" $3 "\x27,\x27" $4 "\x27,\x27" $5 "\x27,\x27" $6 "\x27,\x27" $7 "\x27,\x27" $8 "\x27);"}' tax-values-2018-2019-hybrid.csv > "${OUTPUT}"/tax-values-2018-2019-hybrid.sql

grep -h "${ELECTRIC}" tax-values-2018-2019-filtered.csv | sort -k1 > tax-values-2018-2019-electric.csv
awk -F';' '{print "insert into taxvalues values (default,\x27" $1 "\x27,\x27" $2 "\x27,\x27" substr($3,0,3) "\x27,\x27\Electric\x27,\x27" substr($3,length($3)-4,3) "\x27,\x27" $3 "\x27,\x27" $4 "\x27,\x27" $5 "\x27,\x27" $6 "\x27,\x27" $7 "\x27,\x27" $8 "\x27);"}' tax-values-2018-2019-electric.csv > "${OUTPUT}"/tax-values-2018-2019-electric.sql

cat "${OUTPUT}"/tax-values-2018-2019-petrol.sql "${OUTPUT}"/tax-values-2018-2019-diesel.sql "${OUTPUT}"/tax-values-2018-2019-hybrid.sql "${OUTPUT}"/tax-values-2018-2019-electric.sql > "${OUTPUT}"/tax-values-2018-2019-all.sql

