#!/bin/bash
temppassword=$(awk '/root@localhost:/' /var/log/mysqld.log | awk '{ print $11 }')
mysql --connect-expired-password --password=$temppassword -u root < /tmp/alter_root_user.sql