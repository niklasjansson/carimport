CREATE DATABASE IF NOT EXISTS carimport;
CREATE USER IF NOT EXISTS 'carimport'@'localhost' IDENTIFIED BY '{{ mysql_carimport_pw }}';
GRANT ALL PRIVILEGES ON carimport. * TO 'carimport'@'localhost';
FLUSH PRIVILEGES;