package ax.carimport.domain.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "taxvalues")
//@Document(indexName = "taxdata", type = "tax")
public class Taxdata {

    @Id
    Integer id;
    @Column
    private String manufacturer;
    @Column
    private String model;

    @Column(name = "engine_size")
    private String engineSize;
    @Column(name = "fuel")
    private String fuel;
    @Column(name = "engine_power")
    private String enginePower;
    @Column
    private String specification;
    @Column
    private LocalDate decision_date;
    @Column(name = "registration_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate registrationDate;
    @Column
    private Integer mileage;
    @Column
    private BigDecimal domestic_value;
    @Column
    private BigDecimal vat_amount;

/*
    id INT NOT NULL AUTO_INCREMENT,
    manufacturer VARCHAR(20) NOT NULL,
    model VARCHAR(50) NOT NULL,
    engine_size DECIMAL(2,1) NOT NULL,
    fuel VARCHAR(10) NOT NULL,
    engine_power DECIMAL(4,0) NOT NULL,
    specification VARCHAR(300) NOT NULL,
    decision_date DATE NOT NULL,
    registration_date DATE NOT NULL,
    mileage INT NOT NULL,
    domestic_value DECIMAL(9,2) UNSIGNED NOT NULL,
    vat_amount DECIMAL (9,2) UNSIGNED NOT NULL,
    PRIMARY KEY (id)
 */

    public Taxdata() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public LocalDate getDecision_date() {
        return decision_date;
    }

    public void setDecision_date(LocalDate decision_date) {
        this.decision_date = decision_date;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate    ;
    }

    public BigDecimal getDomestic_value() {
        return domestic_value;
    }

    public void setDomestic_value(BigDecimal domestic_value) {
        this.domestic_value = domestic_value;
    }

    public BigDecimal getVat_amount() {
        return vat_amount;
    }

    public void setVat_amount(BigDecimal vat_amount) {
        this.vat_amount = vat_amount;
    }

    public String getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(String engineSize) {
        this.engineSize = engineSize;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getEnginePower() {
        return enginePower;
    }

    public void setEnginePower(String enginePower) {
        this.enginePower = enginePower;
    }
}
