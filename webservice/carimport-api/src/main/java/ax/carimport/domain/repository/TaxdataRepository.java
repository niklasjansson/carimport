package ax.carimport.domain.repository;

import ax.carimport.domain.model.Taxdata;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

@RepositoryRestResource(collectionResourceRel = "taxdatas", path = "taxdatas")
public interface TaxdataRepository extends PagingAndSortingRepository<Taxdata, Integer> {
    Page<Taxdata> findByManufacturer(@Param("manufacturer") String manufacturer, Pageable p);

    Page<Taxdata> findByManufacturerAndModelAndMileageIsLessThan(
            @Param("manufacturer") String manufacturer,
            @Param("model") String model,
            @Param("mileage") Integer mileage,
            Pageable p);

    Page<Taxdata> findByManufacturerContainingAndModelContaining(
            @Param("manufacturer") String manufacturer,
            @Param("model") String model,
            Pageable p);

    Page<Taxdata> findByManufacturerContainingAndModelContainingAndRegistrationDateIsBetween(
            @Param("manufacturer") String manufacturer,
            @Param("model") String model,
            @Param("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @Param("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
            Pageable p);

    Page<Taxdata> findByManufacturerContainingAndModelContainingAndFuel(
            @Param("manufacturer") String manufacturer,
            @Param("model") String model,
            @Param("fuel") String fuel,
            Pageable p);

    Page<Taxdata> findByManufacturerContainingAndModelContainingAndFuelAndRegistrationDateIsBetween(
            @Param("manufacturer") String manufacturer,
            @Param("model") String model,
            @Param("fuel") String fuel,
            @Param("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @Param("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
            Pageable p);

    Page<Taxdata> findByManufacturerContainingAndModelContainingAndMileageIsLessThanAndFuel(
            @Param("manufacturer") String manufacturer,
            @Param("model") String model,
            @Param("mileage") Integer mileage,
            @Param("fuel") String fuel,
            Pageable p);

    @Query(
            value = "SELECT " +
                    "AVG(domestic_value) as avgDomesticValue, " +
                    "AVG(vat_amount) as avgVatAmount, " +
                    "MAX(domestic_value) as maxDomesticValue," +
                    "MAX(vat_amount) as maxVatAmount," +
                    "MIN(domestic_value) as minDomesticValue," +
                    "MIN(vat_amount) as minVatAmount," +
                    "count(*) as numberOfHits " +
                    "FROM taxvalues WHERE manufacturer like %?1% " +
                    "and model like %?2% " +
                    "and fuel = ?3 " +
                    "and year(registration_date) = ?4",
            nativeQuery = true)
    IStatistics statisticsByManufacturerAndModelAndFuelAndYear(
            @Param("manufacturer") String manufacturer,
            @Param("model") String model,
            @Param("fuel") String fuel,
            @Param("year") String year);

    @Query(
            value = "SELECT " +
                    "AVG(domestic_value) as avgDomesticValue, " +
                    "AVG(vat_amount) as avgVatAmount, " +
                    "MAX(domestic_value) as maxDomesticValue," +
                    "MAX(vat_amount) as maxVatAmount," +
                    "MIN(domestic_value) as minDomesticValue," +
                    "MIN(vat_amount) as minVatAmount," +
                    "count(*) as numberOfHits " +
                    "FROM taxvalues WHERE manufacturer like %?1% " +
                    "and model like %?2% " +
                    "and fuel = ?3 ",
            nativeQuery = true)
    IStatistics statisticsByManufacturerAndModelAndFuel(
            @Param("manufacturer") String manufacturer,
            @Param("model") String model,
            @Param("fuel") String fuel);

    @Query(
            value = "SELECT " +
                    "AVG(domestic_value) as avgDomesticValue, " +
                    "AVG(vat_amount) as avgVatAmount, " +
                    "MAX(domestic_value) as maxDomesticValue," +
                    "MAX(vat_amount) as maxVatAmount," +
                    "MIN(domestic_value) as minDomesticValue," +
                    "MIN(vat_amount) as minVatAmount," +
                    "count(*) as numberOfHits " +
                    "FROM taxvalues WHERE manufacturer like %?1% " +
                    "and model like %?2% " +
                    "and year(registration_date) = ?3",
            nativeQuery = true)
    IStatistics statisticsByManufacturerAndModelAndYear(
            @Param("manufacturer") String manufacturer,
            @Param("model") String model,
            @Param("year") String year);

    @Query(
            value = "SELECT " +
                    "AVG(domestic_value) as avgDomesticValue, " +
                    "AVG(vat_amount) as avgVatAmount, " +
                    "MAX(domestic_value) as maxDomesticValue," +
                    "MAX(vat_amount) as maxVatAmount," +
                    "MIN(domestic_value) as minDomesticValue," +
                    "MIN(vat_amount) as minVatAmount," +
                    "count(*) as numberOfHits " +
                    "FROM taxvalues WHERE manufacturer like %?1% " +
                    "and model like %?2% ",
            nativeQuery = true)
    IStatistics statisticsByManufacturerAndModel(
            @Param("manufacturer") String manufacturer,
            @Param("model") String model);

    public static interface IStatistics {

        BigDecimal getAvgDomesticValue();

        BigDecimal getAvgVatAmount();

        BigDecimal getMinDomesticValue();

        BigDecimal getMaxDomesticValue();

        BigDecimal getMinVatAmount();

        BigDecimal getMaxVatAmount();

        Integer getNumberOfHits();

    }
}

