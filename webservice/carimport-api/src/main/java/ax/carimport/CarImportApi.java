package ax.carimport;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.convert.Jsr310Converters;

@SpringBootApplication
@EntityScan(basePackageClasses = {CarImportApi.class, Jsr310Converters.class})
public class CarImportApi {

    @Autowired
    private ObjectMapper objectMapper;

    public static void main(String... args) {
        SpringApplication.run(CarImportApi.class, args);
    }


}
