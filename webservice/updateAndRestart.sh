#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/application/carimport/

# clone the repo again
git clone https://gitlab.com/niklasjansson/carimport.git

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH

cd /home/application/carimport/ansible

#install npm packages
echo "Running npm install"
sh /home/application/carimport/ansible/deploy.sh
