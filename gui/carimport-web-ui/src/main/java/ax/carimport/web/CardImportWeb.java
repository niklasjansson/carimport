package ax.carimport.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardImportWeb {
    public static void main(String... args) {
        SpringApplication.run(CardImportWeb.class, args);
    }

}
