package ax.carimport.web.model;

import lombok.Data;

@Data
public class SearchForm {
    private String manufacturer;
    private String model;
    private String fuel;
    private String yearModel;
}
