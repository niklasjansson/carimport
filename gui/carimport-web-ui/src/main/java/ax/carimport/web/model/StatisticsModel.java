package ax.carimport.web.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StatisticsModel {
    private BigDecimal avgVatAmount;
    private BigDecimal minVatAmount;
    private BigDecimal maxVatAmount;
    private BigDecimal avgDomesticValue;
    private BigDecimal minDomesticValue;
    private BigDecimal maxDomesticValue;
    private Integer numberOfHits;
}
