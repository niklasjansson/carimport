package ax.carimport.web.controller;

import ax.carimport.web.model.Page;
import ax.carimport.web.model.PagerModel;
import ax.carimport.web.model.SearchForm;
import ax.carimport.web.model.StatisticsModel;
import ax.carimport.web.model.TaxValue;
import ax.carimport.web.model.TaxValuesResponse;
import ax.carimport.web.service.ApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class MainController {
    private static final int INITIAL_PAGE_SIZE = 25;
    private static final int INITIAL_PAGE = 0;
    private static final int[] PAGE_SIZES = {5, 10, 25, 50};
    private int buttonsToShow = 5;

    @Autowired
    ApiService apiService;

    @GetMapping("/")
    public String homePage(Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int evalPageSize = size.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        TaxValuesResponse taxValuesResponse = apiService.getPaginatedData(evalPageSize, evalPage);
        List<TaxValue> paginatedData = taxValuesResponse.getTaxValues();
        PagerModel pagermodel = createPagerModel(evalPage, taxValuesResponse.getPageInformation());
        model.addAttribute("taxvalues", paginatedData);
        model.addAttribute("pageInformation", taxValuesResponse.getPageInformation());
        model.addAttribute("selectedPageSize", evalPageSize);
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("statistics", new StatisticsModel());

        model.addAttribute("pager", pagermodel);
        model.addAttribute("pageSizes", PAGE_SIZES);
        return "home";
    }

    private PagerModel createPagerModel(
            int page, Page pageInformation) {
        return new PagerModel(pageInformation.getTotalPages(), page, buttonsToShow);
    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }
    @GetMapping("/releasenotes")
    public String releasenotes() {
        return "release_notes";
    }

    @GetMapping("/search")
    public String searchWithGet(@RequestParam("manufacturer") String manufacturer,
            @RequestParam("model") String searchModel,
            @RequestParam("fuel") Optional<String> fuel,
            @RequestParam("yearModel") Optional<String> yearModel,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size, Model model) {

        int evalPageSize = size.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        TaxValuesResponse taxValuesResponse = new TaxValuesResponse(new ArrayList<>(), new Page());
        SearchForm searchForm = new SearchForm();
        searchForm.setFuel(fuel.orElse(""));
        searchForm.setManufacturer(manufacturer);
        searchForm.setModel(searchModel);
        searchForm.setYearModel(yearModel.orElse(""));
        StatisticsModel statisticsModel = new StatisticsModel();

        if ((!fuel.isPresent() || fuel.get().isEmpty())
                && (!yearModel.isPresent() || yearModel.get().isEmpty())) {
            taxValuesResponse = searchManufacturerAndModel(manufacturer, searchModel, evalPageSize, evalPage);
            statisticsModel = searchManufacturerAndModel(searchForm.getManufacturer(), searchForm.getModel());
        }
        else if (fuel.isPresent() && !fuel.get().isEmpty()) {
            //Fuel is set
            if ((yearModel.isPresent() && !yearModel.get().isEmpty())) {
                //Fuel and year set
                taxValuesResponse =
                        searchManufacturerAndModelAndFuelAndRegistrationDate(evalPageSize, evalPage,
                                searchForm.getManufacturer(), searchForm.getModel(),
                                searchForm.getFuel(), searchForm.getYearModel());
                statisticsModel =
                        searchManufacturerAndModelAndFuelAndYearModel(searchForm.getManufacturer(),
                                searchForm.getModel(),
                                searchForm.getFuel(), searchForm.getYearModel());
            }
            else {
                //Fuel is only set, no year
                taxValuesResponse =
                        searchManufacturerAndModelAndFuel(searchForm.getManufacturer(), searchForm.getModel(),
                                searchForm.getFuel(), evalPageSize, evalPage);
                statisticsModel =
                        searchManufacturerAndModelAndFuel(searchForm.getManufacturer(), searchForm.getModel(),
                                searchForm.getFuel());
            }
        }
        else if (yearModel.isPresent() && !yearModel.get().isEmpty()) {
            //Year is set
            if (fuel.isPresent() && !fuel.get().isEmpty()) {
                //Year and fuel is set
                taxValuesResponse =
                        searchManufacturerAndModelAndFuelAndRegistrationDate(evalPageSize, evalPage,
                                searchForm.getManufacturer(), searchForm.getModel(),
                                searchForm.getFuel(), searchForm.getYearModel());
                statisticsModel =
                        searchManufacturerAndModelAndFuelAndYearModel(searchForm.getManufacturer(),
                                searchForm.getModel(),
                                searchForm.getFuel(), searchForm.getYearModel());
            }
            else {
                //Year is set, no fuel
                taxValuesResponse =
                        searchManufacturerAndModelAndRegistrationDate(evalPageSize, evalPage,
                                searchForm.getManufacturer(), searchForm.getModel(), searchForm.getYearModel());
                statisticsModel =
                        searchManufacturerAndModelAndYearModel(searchForm.getManufacturer(), searchForm.getModel(),
                                searchForm.getYearModel());
            }

        }
        model.addAttribute("statistics", statisticsModel);
        model.addAttribute("taxvalues", taxValuesResponse.getTaxValues());
        model.addAttribute("searchForm", searchForm);
        model.addAttribute("selectedPageSize", evalPageSize);
        model.addAttribute("pageInformation", taxValuesResponse.getPageInformation());
        model.addAttribute("pager", createPagerModel(evalPage, taxValuesResponse.getPageInformation()));
        model.addAttribute("pageSizes", PAGE_SIZES);

        return "search";
    }

    private TaxValuesResponse searchManufacturerAndModelAndFuel(
            SearchForm searchForm, int size, int page) {
        TaxValuesResponse taxValuesResponse =
                apiService.getFilteredData(size, page, searchForm.getManufacturer(), searchForm.getModel(),
                        searchForm.getFuel());
        return taxValuesResponse;
    }

    private TaxValuesResponse searchManufacturerAndModelAndFuelAndRegistrationDate(
            int size, int page, String manufacturer, String model, String fuel, String year) {
        TaxValuesResponse taxValuesResponse =
                apiService.getFilteredDataWithYear(size, page, manufacturer, model,
                        fuel, year);
        return taxValuesResponse;
    }

    private TaxValuesResponse searchManufacturerAndModelAndRegistrationDate(
            int size, int page, String manufacturer, String model, String year) {
        TaxValuesResponse taxValuesResponse =
                apiService.getFilteredDataWithYear(size, page, manufacturer, model,
                        year);
        return taxValuesResponse;
    }

    private TaxValuesResponse searchManufacturerAndModel(
            SearchForm searchForm, int size, int page) {
        TaxValuesResponse taxValuesResponse =
                apiService.getFilteredData(size, page, searchForm.getManufacturer(), searchForm.getModel());
        return taxValuesResponse;
    }

    private StatisticsModel searchManufacturerAndModel(String manufacturer, String model) {
        StatisticsModel taxValuesResponse =
                apiService.getStatistics(manufacturer, model);
        return taxValuesResponse;
    }

    private StatisticsModel searchManufacturerAndModelAndFuel(String manufacturer, String model, String fuel) {
        StatisticsModel taxValuesResponse =
                apiService.getStatistics(manufacturer, model, fuel);
        return taxValuesResponse;
    }

    private StatisticsModel searchManufacturerAndModelAndFuelAndYearModel(String manufacturer, String model,
            String fuel, String yearModel) {
        StatisticsModel taxValuesResponse =
                apiService.getStatistics(manufacturer, model, fuel, yearModel);
        return taxValuesResponse;
    }

    private StatisticsModel searchManufacturerAndModelAndYearModel(String manufacturer, String model,
            String yearModel) {
        StatisticsModel taxValuesResponse =
                apiService.getStatisticsWithYear(manufacturer, model, yearModel);
        return taxValuesResponse;
    }

    private TaxValuesResponse searchManufacturerAndModelAndFuel(
            String manufacturer, String model, String fuel, int size, int page) {
        TaxValuesResponse taxValuesResponse =
                apiService.getFilteredData(size, page, manufacturer, model,
                        fuel);
        return taxValuesResponse;
    }

    private TaxValuesResponse searchManufacturerAndModel(
            String manufacturer, String model, int size, int page) {
        TaxValuesResponse taxValuesResponse =
                apiService.getFilteredData(size, page, manufacturer, model);
        return taxValuesResponse;
    }

}

