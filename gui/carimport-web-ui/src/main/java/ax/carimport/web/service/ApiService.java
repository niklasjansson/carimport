package ax.carimport.web.service;

import ax.carimport.web.model.StatisticsModel;
import ax.carimport.web.model.TaxValuesResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;

@Slf4j
@Service
public class ApiService {
    @Value("${carimport.api.baseurl}")
    private String apiBaseUrl;
    @Value("${carimport.api.data}")
    private String data;
    @Value("${carimport.api.search.findByManufacturerAndModel}")
    private String findByManufacturerAndModel;
    @Value("${carimport.api.search.findByManufacturerAndModelAndFuel}")
    private String findByManufacturerAndModelAndFuel;
    @Value("${carimport.api.search.findByManufacturer}")
    private String findByManufacturer;

    @Value("${carimport.api.search.statisticsByManufacturerAndModel}")
    private String statisticsByManufacturerAndModel;
    @Value("${carimport.api.search.statisticsByManufacturerAndModelAndFuel}")
    private String statisticsByManufacturerAndModelAndFuel;
    @Value("${carimport.api.search.statisticsByManufacturerAndModelAndFuelAndYear}")
    private String statisticsByManufacturerAndModelAndFuelAndYear;

    @Value("${carimport.api.search.findByManufacturerAndModelAndFuelAndRegistrationDateIsBetween}")
    private String findByManufacturerAndModelAndFuelAndRegistrationDateIsBetween;

    @Value("${carimport.api.search.findByManufacturerAndModelAndRegistrationDateIsBetween}")
    private String findByManufacturerAndModelAndRegistrationDateIsBetween;

    @Value("${carimport.api.search.statisticsByManufacturerAndModelAndYear}")
    private String statisticsByManufacturerAndModelAndYear;

    private final RestTemplate restTemplate;

    public ApiService() {
        this.restTemplate = new RestTemplateBuilder().build();
    }

    public TaxValuesResponse getPaginatedData(int size, int page) {
        URI uri = UriComponentsBuilder.fromHttpUrl(apiBaseUrl).path(data)
                .queryParam("size", size)
                .queryParam("page", page)
                .queryParam("sort", "mileage,asc").build().toUri();
        ResponseEntity<TaxValuesResponse> exchange =
                restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()),
                        TaxValuesResponse.class);
        return exchange.getBody();

    }

    public TaxValuesResponse getFilteredData(int size, int page, String manufacturer, String model) {
        URI uri = UriComponentsBuilder.fromHttpUrl(apiBaseUrl)
                .path(data)
                .path("/")
                .path(findByManufacturerAndModel)
                .queryParam("manufacturer", manufacturer)
                .queryParam("model", model)
                .queryParam("size", size)
                .queryParam("page", page)
                .queryParam("sort", "mileage,asc").build().toUri();

        ResponseEntity<TaxValuesResponse> exchange =
                restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()),
                        TaxValuesResponse.class);
        return exchange.getBody();

    }

    public TaxValuesResponse getFilteredData(int size, int page, String manufacturer, String model, String fuel) {
        URI uri = UriComponentsBuilder.fromHttpUrl(apiBaseUrl)
                .path(data)
                .path("/")
                .path(findByManufacturerAndModelAndFuel)
                .queryParam("manufacturer", manufacturer)
                .queryParam("model", model)
                .queryParam("fuel", fuel)
                .queryParam("size", size)
                .queryParam("page", page)
                .queryParam("sort", "mileage,asc").build().toUri();
        log.info("is {}", uri.toString());
        ResponseEntity<TaxValuesResponse> exchange =
                restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()),
                        TaxValuesResponse.class);
        return exchange.getBody();
    }

    public TaxValuesResponse getFilteredDataWithYear(int size, int page, String manufacturer, String model, String fuel,
            String yearModel) {
        URI uri = UriComponentsBuilder.fromHttpUrl(apiBaseUrl)
                .path(data)
                .path("/")
                .path(findByManufacturerAndModelAndFuelAndRegistrationDateIsBetween)
                .queryParam("manufacturer", manufacturer)
                .queryParam("model", model)
                .queryParam("fuel", fuel)
                .queryParam("startDate", LocalDate.of(Integer.valueOf(yearModel), 1, 1))
                .queryParam("endDate", LocalDate.of(Integer.valueOf(yearModel), 12, 31))
                .queryParam("size", size)
                .queryParam("page", page)
                .queryParam("sort", "mileage,asc").build().toUri();
        log.info("is {}", uri.toString());
        ResponseEntity<TaxValuesResponse> exchange =
                restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()),
                        TaxValuesResponse.class);
        return exchange.getBody();
    }

    public TaxValuesResponse getFilteredDataWithYear(int size, int page, String manufacturer, String model,
            String yearModel) {
        URI uri = UriComponentsBuilder.fromHttpUrl(apiBaseUrl)
                .path(data)
                .path("/")
                .path(findByManufacturerAndModelAndRegistrationDateIsBetween)
                .queryParam("manufacturer", manufacturer)
                .queryParam("model", model)
                .queryParam("startDate", LocalDate.of(Integer.valueOf(yearModel), 1, 1))
                .queryParam("endDate", LocalDate.of(Integer.valueOf(yearModel), 12, 31))
                .queryParam("size", size)
                .queryParam("page", page)
                .queryParam("sort", "mileage,asc").build().toUri();
        log.info("is {}", uri.toString());
        ResponseEntity<TaxValuesResponse> exchange =
                restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()),
                        TaxValuesResponse.class);
        return exchange.getBody();
    }

    public StatisticsModel getStatistics(String manufacturer, String model) {
        URI uri = UriComponentsBuilder.fromHttpUrl(apiBaseUrl)
                .path(data)
                .path("/")
                .path(statisticsByManufacturerAndModel)
                .queryParam("manufacturer", manufacturer)
                .queryParam("model", model)
                .build().toUri();
        log.info("Url is {}", uri.toString());
        ResponseEntity<StatisticsModel> exchange =
                restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()),
                        StatisticsModel.class);
        log.info("Url is {} and result was {}", uri.toString(), exchange.getBody().getNumberOfHits());
        return exchange.getBody();
    }

    public StatisticsModel getStatistics(String manufacturer, String model, String fuel) {
        URI uri = UriComponentsBuilder.fromHttpUrl(apiBaseUrl)
                .path(data)
                .path("/")
                .path(statisticsByManufacturerAndModelAndFuel)
                .queryParam("manufacturer", manufacturer)
                .queryParam("model", model)
                .queryParam("fuel", fuel)
                .build().toUri();
        log.info("Url is {}", uri.toString());
        ResponseEntity<StatisticsModel> exchange =
                restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()),
                        StatisticsModel.class);
        log.info("Url is {} and result was {}", uri.toString(), exchange.getBody().getNumberOfHits());
        return exchange.getBody();
    }

    public StatisticsModel getStatisticsWithYear(String manufacturer, String model, String yearModel) {
        URI uri = UriComponentsBuilder.fromHttpUrl(apiBaseUrl)
                .path(data)
                .path("/")
                .path(statisticsByManufacturerAndModelAndYear)
                .queryParam("manufacturer", manufacturer)
                .queryParam("model", model)
                .queryParam("year", yearModel)
                .build().toUri();
        log.info("Url is {}", uri.toString());
        ResponseEntity<StatisticsModel> exchange =
                restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()),
                        StatisticsModel.class);
        log.info("Url is {} and result was {}", uri.toString(), exchange.getBody().getNumberOfHits());
        return exchange.getBody();
    }

    public StatisticsModel getStatistics(String manufacturer, String model, String fuel, String yearModel) {
        URI uri = UriComponentsBuilder.fromHttpUrl(apiBaseUrl)
                .path(data)
                .path("/")
                .path(statisticsByManufacturerAndModelAndFuelAndYear)
                .queryParam("manufacturer", manufacturer)
                .queryParam("model", model)
                .queryParam("fuel", fuel)
                .queryParam("year", yearModel)
                .build().toUri();
        log.info("Url is {}", uri.toString());
        ResponseEntity<StatisticsModel> exchange =
                restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()),
                        StatisticsModel.class);
        log.info("Url is {} and result was {}", uri.toString(), exchange.getBody().getNumberOfHits());
        return exchange.getBody();
    }

    public TaxValuesResponse getFilteredData(int size, int page, String manufacturer) {
        URI uri = UriComponentsBuilder.fromHttpUrl(apiBaseUrl)
                .path(data)
                .path("/")
                .path(findByManufacturer)
                .queryParam("manufacturer", manufacturer)
                .queryParam("size", size)
                .queryParam("page", page)
                .queryParam("sort", "mileage,asc").build().toUri();

        ResponseEntity<TaxValuesResponse> exchange =
                restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()),
                        TaxValuesResponse.class);
        log.info("Url is {} and result was {}", uri.toString(), exchange.getBody().getTaxValues().size());
        return exchange.getBody();

    }
}
