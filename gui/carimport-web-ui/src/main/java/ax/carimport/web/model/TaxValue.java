package ax.carimport.web.model;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class TaxValue {
    Integer id;
    private String manufacturer;
    private String model;
    private String engineSize;
    private String fuel;
    private String enginePower;
    private String specification;
    private LocalDate decision_date;
    private LocalDate registrationDate;
    private Integer mileage;
    private BigDecimal domestic_value;
    private BigDecimal vat_amount;
}

