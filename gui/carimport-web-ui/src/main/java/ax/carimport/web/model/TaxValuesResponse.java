package ax.carimport.web.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

public class TaxValuesResponse {
    @Getter
    private List<TaxValue> taxValues;

    @Getter
    private Page pageInformation;
    @JsonCreator
    public TaxValuesResponse(@JsonProperty("content") List<TaxValue> taxValues,
            @JsonProperty("page") Page page) {
        this.taxValues = taxValues;
        this.pageInformation = page;
    }

}
